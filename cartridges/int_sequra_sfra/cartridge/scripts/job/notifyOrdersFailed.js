/* eslint-disable consistent-return */

'use strict';

/*
 * Script to run Sequra related jobs
 */

/* API Includes */
var logger = require('dw/system/Logger').getLogger('Sequra', 'Sequra');
var Order = require('dw/order/Order');

/**
 * notifyOrdersFailed - Find Orders paid and Failed and send an email to inform notification email.
 */
function notifyOrdersFailed() {
    logger.debug('synchronizeSequraPayments Process');
    var emailHelpers = require('*/cartridge/scripts/helpers/emailHelpers');
    var Site = require('dw/system/Site');

    var orderDateAllowed = new dw.util.Calendar();
    orderDateAllowed.add(dw.util.Calendar.DAY_OF_YEAR, -1);

    var orders = dw.order.OrderMgr.searchOrders(
        'status = {0} AND custom.sequra_TransactionID != {1} AND paymentStatus = {2} AND lastModified >= {3}',
        'creationDate desc',
        Order.ORDER_STATUS_FAILED,
        null,
        Order.PAYMENT_STATUS_PAID,
        orderDateAllowed.getTime()
    );

    var arrayOrdersIds = [];

    var email = Site.current.getCustomPreferenceValue('sequra_notificationEmail');
    if (email) {
        var emailObj = {
            to: email,
            subject: dw.web.Resource.msg('subject.sequra.notification.email', 'sequra', null),
            from: Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@testorganization.com'
        };
        while (orders.hasNext()) {
            var order = orders.next();
            arrayOrdersIds.push(order.getOrderNo());
        }

        if (arrayOrdersIds.length > 0) {
            var objectForEmail = {
                arrayOrdersIds: arrayOrdersIds
            };

            emailHelpers.sendEmail(emailObj, 'sequra/email/orderFailedNotificationEmail', objectForEmail);
        }
    } else {
        logger.error('notifyOrdersFailed error : Not sequra_notificationEmail set');
        return new dw.system.Status(dw.system.Status.ERROR);
    }
    orders.close();

    return new dw.system.Status(dw.system.Status.OK);
}

module.exports = {
    notifyOrdersFailed: notifyOrdersFailed
};
