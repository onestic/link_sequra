
function loadSequraWidget() {
    if (typeof Sequra !== 'undefined') {
        // eslint-disable-next-line no-undef
        Sequra.onLoad(() => {
        // eslint-disable-next-line no-undef
            Sequra.refreshComponents();
        });
    }
}

/**
 * Returns an object containing decimal and thousand separator
 * based on user's locale
 */
function getNumberSeparators(number) {
    const res = {
        decimal: '.',
        thousand: ',',
    };

    if (number) {
        // convert a number formatted according to locale
        const str = number.toLocaleString();

        const commaPosition = str.indexOf(',');
        const dotPosition = str.indexOf('.');

        if (commaPosition !== -1 && dotPosition === -1) {
            res.decimal = ',';
            res.thousand = '.';
        } else if (commaPosition === -1 && dotPosition !== -1) {
            res.decimal = '.';
            res.thousand = ',';
        } else if (commaPosition !== -1 && dotPosition !== -1 && commaPosition > dotPosition) {
            res.decimal = ',';
            res.thousand = '.';
        } else if (commaPosition !== -1 && dotPosition !== -1 && commaPosition < dotPosition) {
            res.decimal = '.';
            res.thousand = ',';
        }
    }
    return res;
}

// File extended for use the 'availability' method in ./base file of pagantis cartridge
module.exports = {
    loadSequraWidget,

    renderSequraWidget(amount, quantity, $component, formatprice) {
        const data = {
            amount,
            quantity,
        };
        const separators = getNumberSeparators(formatprice);
        if (window.sequraConfigParams) {
            window.sequraConfigParams.decimalSeparator = separators.decimal;
            window.sequraConfigParams.thousandSeparator = separators.thousand;
        }
        $.ajax({
            // eslint-disable-next-line no-undef
            url: window.sequraURLLoad.loadURL,
            type: 'get',
            dataType: 'json',
            data,
            success(response) {
                const $setProducts = $('body').find('.set-item');
                const $widgetComponent = $setProducts.length ? $component.find('.sequra-widget') : $('body').find('.sequra-widget');
                if ($widgetComponent.length) {
                    $widgetComponent.remove();
                }
                const extendwidgetClass = $setProducts.length ? ' col-sm-12' : '';
                const componentClass = `sequra-widget${extendwidgetClass}`;
                $component.append(`<div class="${componentClass}">${response.sequraWidgetTemplate}</div>`);
                loadSequraWidget();
            },
            error() {

            },
        });
    },

};
